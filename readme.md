# High-Contrast Display Module

Source files for the project. The PDF file contains a report about this project.


Software used:

* Solidworks

* National Instruments Multisim

* National Instruments Ultiboard


![Assembled prototype, with some DFM improvements](Preview.png)